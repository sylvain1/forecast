const gulp           = require('gulp');
const plumber        = require('gulp-plumber');
const concat         = require('gulp-concat');
const stylus         = require('gulp-stylus');
const rupture        = require('rupture');
const sourcemaps     = require('gulp-sourcemaps');
const size           = require('gulp-size');
const postcss        = require('gulp-postcss');
const defaultunit    = require('postcss-default-unit');
const circle         = require('postcss-circle');
const crip           = require('postcss-crip');
const csssize        = require('postcss-size');
const font_magician  = require('postcss-font-magician');
const pxtorem        = require('postcss-pxtorem');
const shortfontsize  = require('postcss-short-font-size');
const shortcolor     = require('postcss-short-color');
const colorshort     = require('postcss-color-short');
const ie_opacity     = require('postcss-opacity');
const center         = require('postcss-center');
const position       = require('postcss-position-alt');
const initial        = require('postcss-initial');
const fontweights    = require('postcss-font-weights');
const flexbugs       = require('postcss-flexbugs-fixes');
const border         = require('postcss-border-shortcut');
const animation      = require('postcss-animation');
const animationmagic = require('postcss-magic-animations');
const axis           = require('postcss-axis');
const autoprefixer   = require('autoprefixer');
const charset        = require('postcss-normalize-charset');
const mqpacker       = require('css-mqpacker');
const csswring       = require('csswring');

var path = {
  css: ['src/css/import/all.styl'],
};

var onError = function (err) {
  require('gulp-util').beep();
  console.log(err);
};

gulp.task('css', function(done) {
  var processors = [
    crip,
    circle,
    csssize,
    axis,
    initial({reset: 'inherited', replace: true}),
    position,
    center,
    shortcolor,
    colorshort,
    shortfontsize,
    fontweights,
    defaultunit({ unit: 'px' }),
    border,
    animation,
    animationmagic,
    pxtorem({
      rootValue: 10,
      unitPrecision: 5,
      propWhiteList: [],
      selectorBlackList: ['html'],
      replace: true,
      mediaQuery: false,
      minPixelValue: 2
    }),
    font_magician({
      foundries: ['custom', 'hosted', 'google']
    }),
    flexbugs,
    autoprefixer({browsers: ['last 3 versions'], cascade: false}),
    charset,
    mqpacker,
    csswring({removeAllComments: true, map: false})
  ];

  gulp.src(path.css)
  .pipe(plumber({errorHandler: onError}))
  .pipe(sourcemaps.init())
  .pipe(stylus({
    use: [ rupture() ],
    compress: true,
  }))
  .pipe(postcss(processors))
  .pipe(concat('all.css'))
  .pipe(gulp.dest('src/css'))
  .pipe(sourcemaps.write('.'))
  .pipe(size({showFiles:true}));
  done();
});

gulp.task('watch', function(done) {
  gulp.watch('src/css/**/*.styl', gulp.parallel('css'));
  done();
});

gulp.task('default', gulp.parallel('css', 'watch', function(done){
  done();
}));

