import { fromJS } from 'immutable';
import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_FAILED,
} from './constants';

const intialState = fromJS({
  isFetching: false,
  data: [],
  country: '',
});

export default function(state = intialState, action) {
  switch (action.type) {
    case FETCH_FORECAST: {
      return state.set('isFetching', true);
    }
    case FETCH_FORECAST_SUCCESS: {
      return state
        .merge({
          data: fromJS(action.data),
          country: action.country,
          isFetching: false,
        });
    }
    case FETCH_FORECAST_FAILED: {
      console.log(action.error);
      return state.set('isFetching', false);
    }
    default: {
      return state;
    }
  }
}
