import { createSelector } from 'reselect';

/**
 * Direct selector to the forecast state
 */
const selectForecast = () => (state) => state.get('forecast');

/**
 * Selector to country
 */
const selectCountry = () => createSelector(
  selectForecast(),
  (substate) => substate.get('country')
);

/**
 * Selector to isFetching
 */
const selectIsFetching = () => createSelector(
  selectForecast(),
  (substate) => substate.get('isFetching')
);

/**
 * Selector to list
 */
const selectData = () => createSelector(
  selectForecast(),
  (substate) => substate.get('data').toJS()
);

export {
  selectCountry,
  selectData,
  selectIsFetching,
};
