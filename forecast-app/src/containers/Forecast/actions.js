import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_FAILED,
} from './constants';

export function fetchForecast() {
  return {
    type: FETCH_FORECAST,
  };
}

export function fetchForecastSuccess(data, country) {
  return {
    data,
    country,
    type: FETCH_FORECAST_SUCCESS,
  };
}

export function fetchForecastFailed(error) {
  return {
    error,
    type: FETCH_FORECAST_FAILED,
  };
}
