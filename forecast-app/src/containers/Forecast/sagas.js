import { takeLatest, call, put, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import request from '../../utils/request';
import { selectCoordinates } from '../Search/selectors';
import {
  fetchForecast,
  fetchForecastSuccess,
  fetchForecastFailed,
} from './actions';
import {
  FETCH_FORECAST,
  API_URL_BASE,
  API_KEY,
} from './constants';

/**
 * Root saga manages watcher lifecycle
 */
function* watchFetchForecastSaga() {
  yield takeLatest(FETCH_FORECAST, fetchForecastSaga);
}

function* fetchForecastSaga() {
  const { lat, lng } = yield select(selectCoordinates());

  const url = `${API_URL_BASE}lat=${lat}&lon=${lng}&appid=${API_KEY}`;

  const { data } = yield call(request, url);

  if (!data.err) {
    yield put(fetchForecastSuccess(
      data.list,
      data.city.country,
    ));
  } else {
    yield put(fetchForecastFailed(data.err.message));
  }

  // Reload every minute
  yield call(delay, 60 * 1000);
  yield put(fetchForecast());
}

export default [
  watchFetchForecastSaga,
];
