import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import ForecastWrapper from '../../components/Forecast/ForecastWrapper';
import { fetchForecast } from './actions';
import {
  selectCountry,
  selectData,
  selectIsFetching,
} from './selectors';

function Forecast({
  country,
  data,
  isFetching,
  dispatch,
  handleReload
}) {
  return (
    <ForecastWrapper
      country={country}
      data={data}
      isFetching={isFetching}
      handleReload={handleReload}
    />
  );
}

Forecast.propTypes = {
  country: PropTypes.string,
  data: PropTypes.array,
  isFetching: PropTypes.bool,
  handleReload: PropTypes.func.isRequired,
};

const mapStateToProps = createSelector(
  selectCountry(),
  selectData(),
  selectIsFetching(),
  (
    country,
    data,
    isFetching
  ) => ({
    country,
    data,
    isFetching,
  })
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  handleReload: () => dispatch(fetchForecast()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Forecast);
