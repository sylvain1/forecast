import { fromJS } from 'immutable';
import {
  SET_COORDINATES,
  SET_ADDRESS,
  SET_ERROR,
} from './constants';

const intialState = fromJS({
  address: '',
  error: '',
  coordinates: {
    lat: 0,
    lng: 0,
  },
});

export default function(state = intialState, action) {
  switch (action.type) {
    case SET_ADDRESS: {
      return state.merge({
        address: action.address,
        error: '',
      });
    }
    case SET_ERROR: {
      return state.merge({
        error: action.error,
      });
    }
    case SET_COORDINATES: {
      return state.merge({
        coordinates: action.coordinates,
        error: '',
      });
    }
    default: {
      return state;
    }
  }
}
