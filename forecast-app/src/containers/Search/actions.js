import {
  SET_COORDINATES,
  SET_ADDRESS,
  SET_ERROR,
  REQUEST_LOCATION,
  REQUEST_LOCATION_FROM_ADDRESS,
} from './constants';

export function setCoordinates(coordinates) {
  return {
    coordinates,
    type: SET_COORDINATES,
  };
}

export function setAddress(address) {
  return {
    address,
    type: SET_ADDRESS,
  };
}

export function setError(error) {
  return {
    error,
    type: SET_ERROR,
  };
}

export function requestLocation() {
  return {
    type: REQUEST_LOCATION,
  };
}

export function requestLocationFromAddress(address) {
  return {
    address,
    type: REQUEST_LOCATION_FROM_ADDRESS,
  };
}
