import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { selectAddress, selectError } from './selectors';
import FormSearch from '../../components/FormSearch';
import {
  setAddress,
  setError,
  requestLocation,
  requestLocationFromAddress,
} from './actions';

class Search extends PureComponent {
  static propTypes = {
    address: PropTypes.string,
    error: PropTypes.string,
    setAddress: PropTypes.func.isRequired,
    requestLocation: PropTypes.func.isRequired,
  };

  componentWillMount() {
    if(!this.props.address) {
      this.props.requestLocation();
    } else {
      this.props.dispatch(requestLocationFromAddress());
    }
  }

  onSelect = (address) => {
    this.props.dispatch(setAddress(address));
    this.props.dispatch(requestLocationFromAddress());
  };

  handleFormSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(requestLocationFromAddress());
  };


  render() {
    const inputProps = {
      value: this.props.address,
      onChange: this.props.setAddress,
      placeholder: 'Search Places...',
    };

    return (
      <FormSearch
        handleFormSubmit={this.handleFormSubmit}
        inputProps={inputProps}
        locateMe={this.props.requestLocation}
        onSelect={this.onSelect}
        onError={this.props.onError}
        onEnterKeyDown={this.onSelect}
        error={this.props.error}
      />
    );
  }
}

const mapStateToProps = createSelector(
  selectAddress(),
  selectError(),
  (address, error) => ({ address, error }),
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  setAddress: (address) => dispatch(setAddress(address)),
  requestLocation: () => dispatch(requestLocation()),
  onError: (error) => dispatch(setError(error)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
