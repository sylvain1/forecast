import { takeLatest, call, put, select } from 'redux-saga/effects';
import { geocodeReverse } from '../../utils/geocodeReverse';
import {
  getLatLng,
  geocodeByAddress,
} from 'react-places-autocomplete';
import {
  REQUEST_LOCATION,
  REQUEST_LOCATION_FROM_ADDRESS,
} from './constants';
import {
  setAddress,
  setCoordinates,
} from './actions';
import { selectAddress } from './selectors';
import { fetchForecast } from '../Forecast/actions';

/**
 * Root saga manages watcher lifecycle
 */
function* watchRequestLocationSaga() {
  yield takeLatest(REQUEST_LOCATION, requestLocationSaga);
}

function* requestLocationSaga() {
  const options = {
    enableHighAccuracy: false,
    timeout: 5000,
    maximumAge: 0,
  };

  const { latitude, longitude } = yield call(getCurrentPosition, options);
  const address = yield call(geocodeReverse, latitude, longitude);

  yield put(setAddress(address));
  yield put(setCoordinates({ lat: latitude, lng: longitude }));
  yield put(fetchForecast());
}


function* watchRequestLocationFromAddressSaga() {
  yield takeLatest(
    REQUEST_LOCATION_FROM_ADDRESS,
    requestLocationFromAddressSaga
  );
}

function* requestLocationFromAddressSaga() {
  const address = yield select(selectAddress());

  const results = yield call(geocodeByAddress, address);

  const { lat, lng } = yield call(getLatLng, results[0]);

  yield put(setCoordinates({ lat, lng }));
  yield put(fetchForecast());
}

/**
 * Promisify navigator.geolocation.getCurrentPosition
 */
function getCurrentPosition(options) {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      ({ coords }) => {
        resolve(coords);
      },
      null,
      options
    );
  });
}

export default [
  watchRequestLocationSaga,
  watchRequestLocationFromAddressSaga
];
