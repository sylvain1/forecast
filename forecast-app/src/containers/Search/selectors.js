import { createSelector } from 'reselect';

/**
 * Direct selector to the search state
 */
const selectSearch = () => (state) => state.get('search');

/**
 * Selector to address
 */
const selectAddress = () => createSelector(
  selectSearch(),
  (substate) => substate.get('address')
);

/**
 * Selector to error
 */
const selectError = () => createSelector(
  selectSearch(),
  (substate) => substate.get('error')
);

/**
 * Selector to coordinates
 */
const selectCoordinates = () => createSelector(
  selectSearch(),
  (substate) => substate.get('coordinates').toJS()
);

export {
  selectAddress,
  selectError,
  selectCoordinates,
};
