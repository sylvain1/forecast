export const SET_COORDINATES = 'forecast-app/Search/SET_COORDINATES';
export const SET_ADDRESS = 'forecast-app/Search/SET_ADDRESS';
export const SET_ERROR = 'forecast-app/Search/SET_ERROR';
export const REQUEST_LOCATION = 'forecast-app/Search/REQUEST_LOCATION';
export const REQUEST_LOCATION_FROM_ADDRESS = 'forecast-app/Search/REQUEST_LOCATION_FROM_ADDRESS';
