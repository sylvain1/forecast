import React from 'react';
import PropTypes from 'prop-types';
import PlacesAutocomplete from 'react-places-autocomplete';

function FormSearch({
  inputProps,
  handleFormSubmit,
  onError,
  onSelect,
  onEnterKeyDown,
  locateMe,
  error,
}) {
  const classNames = {
    root: 'search__input-wrapper',
    input: 'search__input',
    autocompleteContainer: 'search__autocomplete',
  };

  let errorMessage = '';
  if(error === 'ZERO_RESULTS') {
    errorMessage = 'No result for this address';
  }

  return (
    <form className="search" onSubmit={handleFormSubmit}>
      <div className="search__wrapper">
        <PlacesAutocomplete
          classNames={classNames}
          debounce={280}
          inputProps={inputProps}
          onSelect={onSelect}
          onEnterKeyDown={onEnterKeyDown}
          onError={onError}
        />
        <button
          className="button"
          type="submit"
        >
          OK
        </button>
        <button
          className="button button--locate"
          onClick={locateMe}
        >
          Locate me
        </button>
      </div>
      <div className="message message--error">
        {errorMessage}
      </div>
    </form>
  );
}

FormSearch.propTypes = {
  inputProps: PropTypes.object.isRequired,
  handleFormSubmit: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onEnterKeyDown: PropTypes.func.isRequired,
  error: PropTypes.string,
};

export default FormSearch;
