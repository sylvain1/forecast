import React from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-spinkit';

function Throbber({ visible }) {
  const throbberCss = visible ? 'visible' : 'hidden';

  return (
    <div className={`throbber throbber--${throbberCss}`}>
      <Spinner
        name="circle"
        fadeIn="none"
        overrideSpinnerClassName="throbber__spinner"
      />
    </div>
  );
}

Throbber.propTypes = {
  visible: PropTypes.bool.isRequired,
};

export default Throbber;
