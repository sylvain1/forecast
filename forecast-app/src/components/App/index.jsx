import React from 'react';
import Search from '../../containers/Search';
import Forecast from '../../containers/Forecast';

function App() {
  return (
    <div className="app">
      <Search />
      <Forecast />
    </div>
  );
}

export default App;
