import React from 'react';
import PropTypes from 'prop-types';
import ForecastFlag from './ForecastFlag';
import ForecastList from './ForecastList';
import Throbber from '../Throbber';

function ForecastWrapper({
  country,
  data,
  handleReload,
  isFetching,
}) {
  return (
    <div className="forecast">
      <Throbber visible={isFetching} />
      <button
        className="button button--turquoise button--small"
        onClick={handleReload}
      >
        Reload
      </button>
      <ForecastFlag country={country} />
      <ForecastList data={data} />
    </div>
  );
}

ForecastWrapper.propTypes = {
  country: PropTypes.string,
  data: PropTypes.array,
  handleReload: PropTypes.func,
  isFetching: PropTypes.bool,
};

export default ForecastWrapper;
