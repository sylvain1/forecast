import React from 'react';
import PropTypes from 'prop-types';
import dateformat from 'dateformat';

function ForecastItem({ item }) {
  const { weather, main: { temp } } = item;
  const { description, id } = weather[0];

  // Kelvin to Celsius
  const tempCelsius = Number(temp - 273).toFixed(1);

  return (
    <div className="forecast__item">
      <div className="forecast__date">
        <div className="forecast__date__day">
          {dateformat(item.dt * 1000, 'dddd dd mmmm yyyy')}
        </div>
        <div className="forecast__date__hour">
          {dateformat(item.dt * 1000, 'HH:MM')}
        </div>
      </div>
      <div className="forecast-icon ">
        <i className={`owf owf-${id}`}/>
      </div>
      <div className="forecast__description">
        {description}
      </div>
      <div className="forecast__temp">
        {tempCelsius}°C
      </div>
    </div>
  );
}

ForecastItem.propTypes = {
  item: PropTypes.object,
};

export default ForecastItem;
