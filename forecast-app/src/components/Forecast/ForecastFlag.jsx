import React from 'react';
import PropTypes from 'prop-types';
import FlagIconFactory from 'react-flag-icon-css';

const FlagIcon = FlagIconFactory(React, { useCssModules: false });

function ForecastFlag({ country }) {

  return (
    <FlagIcon
      className="flag"
      code={country.toLowerCase()}
      size="2x"
    />
  );
}

ForecastFlag.propTypes = {
  country: PropTypes.string,
};

export default ForecastFlag;
