import React from 'react';
import PropTypes from 'prop-types';
import ForecastDay from './ForecastDay';

function getDay(dt) {
  return new Date(dt * 1000).getDate();
}

function getItemsGroupedByDay(data) {
  const itemsByDay = {};

  data.forEach((item) => {
    const day = getDay(item.dt);

    if(!itemsByDay[day]) {
      itemsByDay[day] = [];
    }
    itemsByDay[day].push(item);
  });

  return itemsByDay;
}

function ForecastList({ data }) {
  const itemsByDay = getItemsGroupedByDay(data);

  return (
    <div className="forecast__list">
      {Object.keys(itemsByDay).map((key) => {
        return (
          <ForecastDay
            key={key}
            items={itemsByDay[key]}
          />
        );
      })}
    </div>
  );
}

ForecastList.propTypes = {
  data: PropTypes.array,
};

export default ForecastList;
