import React from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';

function ForecastDay({ items }) {
  return (
    <div className="forecast__day">
      {items.map((item) => {
        return (
          <ForecastItem
            key={item.dt}
            item={item}
          />
        );
      })}
    </div>
  );
}

ForecastDay.propTypes = {
  items: PropTypes.array,
};

export default ForecastDay;
