/**
  Get address from lat, lng using Google map geocoder
*/
export const geocodeReverse = (lat, lng) => {
  const geocoder = new window.google.maps.Geocoder();
  const OK = window.google.maps.GeocoderStatus.OK;

  return new Promise((resolve, reject) => {
    const location = { lat, lng };

    geocoder.geocode({ location }, (results, status) => {
      if (status !== OK || !results[0]) {
        reject(status);
      }

      resolve(results[0].formatted_address);
    });
  });
};
