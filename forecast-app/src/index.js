import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist-immutable';
import configureStore from './store';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import Throbber from './components/Throbber';
import './css/all.css';

const store = configureStore();

export default class AppProvider extends Component {
  constructor() {
    super();
    this.state = { rehydrated: false };
  }

  componentWillMount() {
    persistStore(store, {}, () => {
      this.setState({ rehydrated: true });
    });
  }

  render() {
    return this.state.rehydrated ? (
      <Provider store={store}>
        <App />
      </Provider>
    ) : (
      <Throbber visible={true} />
    );
  }
}

render(
  <AppProvider />,
  document.getElementById('root')
);

registerServiceWorker();
