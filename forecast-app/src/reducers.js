import { combineReducers } from 'redux-immutable';
import SearchReducer from './containers/Search/reducer';
import forecastReducer from './containers/Forecast/reducer';

const reducers = combineReducers({
  search: SearchReducer,
  forecast: forecastReducer,
});

export default reducers;
