### Instructions

« You need to build an application for displaying of forecast at a current location and also on the user’s option (it contains text entering of the city, autocomplete is not need but will be a plus)

If you close tabs and then return to it, the application must show the last version of typed location with an appropriate forecast and also update it. The absence of data is acceptable if you open the application for the first time. If user moved to another location and didn’t use the application, the forecast must be updated in accordance with new location.

The application must be written with a use of React, Redux and must comply with style guide Airbnb.

- https://github.com/airbnb/javascript
- https://github.com/airbnb/javascript/tree/master/react

Layout must be done in BEM-notation, and visually delineate the content. Any frameworks are used. »

### References

- [react-places-autocomplete](https://github.com/kenny-hibino/react-places-autocomplete)
- [redux-persist](https://github.com/rt2zz/redux-persist)
- [openweathermap API](https://openweathermap.org/forecast5) *81b6d1c850a675890c417939f120a70b*
